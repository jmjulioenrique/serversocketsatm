import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;

public class atmController extends Thread {

	private Socket skt;
	private DatagramSocket AlertSocket;
	private InetAddress IPAddress;
	private ServerSocket alertclientsSocket;
	private Socket skttoalerts;

	public atmController(Socket skt, DatagramSocket AlertSocket,
			InetAddress IPAddress, ServerSocket alertclientsSocket) throws Exception {

		this.skt = skt;
		this.AlertSocket = AlertSocket;
		this.IPAddress = IPAddress;
		this.alertclientsSocket = alertclientsSocket;
	}

	public void run() {
		try {
			System.out.println("Cliente conectado");
			
			// set conections
			BufferedReader ent = new BufferedReader(new InputStreamReader(
					skt.getInputStream()));
			PrintWriter sal;

			sal = new PrintWriter(skt.getOutputStream(), true);

			String card = ent.readLine(); // read card number
			Card userAct = getCard(card); // load card from 'database'

			if (userAct != null) {
				System.out.println(userAct.getCredit());
				sal.println("OK");
				boolean sw = false;
				int attempt = 1;

				while (attempt <= Main.totalAttempts && sw == false) {
					String pin = ent.readLine(); // read pin number
					if (Integer.parseInt(pin) == userAct.getPin()) { // check
																		// pin
																		// number
						sw = true;
					} else {
						if (attempt < Main.totalAttempts)
							sal.println("RETRY");
						System.out.println(attempt);
						attempt++;
					}
				}
				if (sw == false)
					sal.println("DENY");
				else {
					sal.println("OK");
					// LOGGED IN										
					//conect to alert socket and add it to map
					skttoalerts = alertclientsSocket.accept();
					Main.alertConectedClients.put(userAct.getNumber(),skttoalerts);
					System.out.println("conectado a socket de alertas con cliente");
					
					// Read operation
					String operation = ent.readLine();
					// parse operation
					String op1 = operation;
					if (operation.indexOf("/") != -1)
						op1 = operation.substring(0, operation.indexOf("/"));

					while (!op1.equals("EXIT")) {

						if (op1.equals("CHECKACCOUNT")) {
							sal.println(userAct.getCredit());
						}

						if (op1.equals("WITHDRAW")) {
							// parse arguments
							String argument = operation.substring(
									operation.indexOf("/") + 1,
									operation.length());
							// check amount
							float amount = Float.parseFloat(argument);
							if (amount <= userAct.getCredit()) {
								userAct.getCash(amount);
								sal.println("SUCCESS");
							} else {
								sal.println("DENY");
								// call alert server
								byte[] sendData = new byte[1024];
								String env = userAct.getNumber();
								sendData = env.getBytes();
								DatagramPacket sendPacket = new DatagramPacket(
										sendData, sendData.length, IPAddress,
										9876);
								AlertSocket.send(sendPacket);
							}
						}

						operation = ent.readLine();
						// parse operation
						op1 = operation;
						if (operation.indexOf("/") != -1)
							op1 = operation
									.substring(0, operation.indexOf("/"));
					} // end options iteration
					
					//close socket
					skttoalerts.close();
					Main.alertConectedClients.remove(userAct.getNumber());
				}
			} else
				sal.println("CANCEL");

			skt.close();
			System.out.println("cliente desconectado");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static Card getCard(String number) {
		Iterator<Card> it = Main.cards.iterator();
		while (it.hasNext()) {
			Card c = it.next();
			if (c.getNumber().equals(number))
				return c;
		}
		return null;
	}
}
