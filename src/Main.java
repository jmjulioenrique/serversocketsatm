import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

	public static int totalAttempts = 3;

	public static List<Card> cards = new ArrayList<Card>();
	
	public static Map<String, Socket> alertConectedClients = new HashMap<String, Socket>();
	
	@SuppressWarnings("resource")
	public static void main(String[] args) throws Exception {

		Card user1 = new Card();
		user1.setNumber("1122334455");
		user1.setPin(1234);
		user1.setCredit(100);
		cards.add(user1);
		
		// tcp socket
		ServerSocket client = new ServerSocket(1234);
		
		//tcp socket for client alerts
		ServerSocket AlertclientsSocket = new ServerSocket(4321);

		// udp socket with alert server
		DatagramSocket AlertServerSocket = new DatagramSocket();
		
		try {
			InetAddress IPAddress = InetAddress.getByName("localhost");
			
			//Alerts Controller
			AlertServerHandler inAlert = new AlertServerHandler(AlertServerSocket);
			inAlert.start();
			
			while (true) {
				Socket skt = client.accept();
				//Client Controllers
				atmController hilo = new atmController(skt, AlertServerSocket, IPAddress , AlertclientsSocket);
				hilo.start();
			}
			
		} catch (Exception e) {
			AlertServerSocket.close();
			System.out.print("Server error \n");
		}

	}

}
