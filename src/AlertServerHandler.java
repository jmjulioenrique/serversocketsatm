import java.io.IOException;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class AlertServerHandler extends Thread {

	private DatagramSocket alertServerSocket;

	public AlertServerHandler(DatagramSocket alertServerSocket) {
		this.alertServerSocket = alertServerSocket;
	}

	public void run() {
		
		while(true){
		try {
			
			byte[] receiveData = new byte[1024];
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			alertServerSocket.receive(receivePacket);
			String warningUser = new String(receivePacket.getData());
			System.out.println("FROM Alert SERVER:" + warningUser);
			
			//send to client conected
			PrintWriter sal;
			String AlertUser = warningUser.substring(0,10);
			System.out.println(AlertUser.length());
			Socket socketClient = Main.alertConectedClients.get(AlertUser);
			sal = new PrintWriter(socketClient.getOutputStream(), true);
			
			String env = "Police are on their way to get you in jail. User: "+ warningUser;
			sal.println(env);
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}

}
