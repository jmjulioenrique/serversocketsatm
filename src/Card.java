
public class Card {

	private String number;
	private int pin;
	private float credit;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}
	public float getCredit() {
		return credit;
	}
	public void setCredit(float credit) {
		this.credit = credit;
	}
	
	public int getCash(float amount){
		if(this.credit - amount >= 0){
			this.credit = this.credit - amount;
			return 0;
		}
		else
			return -1;		
	}
	
}
